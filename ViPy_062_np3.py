import numpy as np
import os
import statistics
import math
from random import randint

os.chdir('D:/English')
data = np.genfromtxt('iris.data', dtype=float, delimiter=",", names=None, encoding=None)
data = np.delete(data, 4, axis=1)
# print(data)
for _ in range(5):
    x = randint(1, 150) - 1
    y = randint(1, 4) - 1
    data[x][y] = np.nan
    print("data[{}][{}] = nan".format(x, y))
print("change Nan", data)
for m in range(0, 150):
    for n in range(0, 4):
        if math.isnan(data[m][n]):
            data[m][n] = 0
print("change 0", data)
