if __name__ == '__main__':
    import numpy as np
    import os
    import statistics
    os.chdir('D:/English')
    data = np.genfromtxt('iris.data', dtype=float, delimiter=",")
    data = np.delete(data, 4, axis=1)
    #print(data)
    x = statistics.mean(data[:, 0])
    print('mean:', x)
    y = statistics.median(data[:, 0])
    print('median:', y)
    z = statistics.stdev(data[:, 0])
    print('Standard Deviation', z)