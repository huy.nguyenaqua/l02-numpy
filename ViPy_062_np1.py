def normalize(a):
    min = np.min(a)
    max = np.max(a)
    range = max - min
    return [(a - min) / range for a in a]
if __name__ == '__main__':
    import numpy as np
    import os
    import statistics
    os.chdir('D:/English')
    data = np.genfromtxt('iris.data', dtype=float, delimiter=",")
    data = np.delete(data, 4, axis=1)
    x = data[:, 0]
    normalizedX = normalize(x)
    print(normalizedX)
