import numpy as np
import os
import statistics

os.chdir('D:/English')
data = np.genfromtxt('iris.data', dtype=float, delimiter=",")
data = np.delete(data, 4, axis=1)

return_data = []

for column in data:
    if column[0] < 5 and column[2] > 1.5:
        return_data.append(column)

print(return_data)