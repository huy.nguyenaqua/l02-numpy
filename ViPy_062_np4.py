import medium as medium
import numpy as np
import os
import statistics
import math
from random import randint

os.chdir('D:/English')
data = np.genfromtxt('iris.data', dtype=float, delimiter=",", names=None, encoding=None)
data = np.delete(data, 4, axis=1)
# print(data)

return_data = []

for i in range(0, 4):
    if data[i][2] < 3:
        return_data.append('small')
    elif 3 <= data[i][2] <= 5:
        return_data.append('medium')
    elif data[i][2] >= 5:
        return_data.append('large')
print(return_data)

